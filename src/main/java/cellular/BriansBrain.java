package cellular;

import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton {
    IGrid currentGeneration;

    public BriansBrain(int i, int i1) {
    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row,column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }


    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        // TODO
        for (int row = 0; row < numberOfRows(); row++) {
            for (int col = 0; col < numberOfColumns(); col++) {
                CellState state = getNextCell(row, col);
                nextGeneration.set(row, col, state);
            }
        }

    }
        private int countNeighbors(int row, int col, CellState state) {
            // TODO
            int count = 0;
            for(int x = row - 1; x <= row + 1; x++) {
                for(int y = col - 1; y <= col + 1; y++) {
                    try {
                        if (getCellState(x, y).equals(state)) {
                            if (x != row || y != col) {
                                count++;
                            }
                        }
                    }
                    catch (IndexOutOfBoundsException e) {
                        System.out.println();
                    }
                }
            }
            return count;
        }


        @Override
        public CellState getNextCell(int row, int col) {
            // TODO
            int count = this.countNeighbors(row, col, CellState.ALIVE);
            CellState currentState = currentGeneration.get(row, col);

            if (currentState == CellState.ALIVE) {
                return CellState.DYING;
            } else if (currentState == CellState.DYING){
                return CellState.DEAD;
            }
            else if (currentState == CellState.DEAD){
                if(count == 2){
                    return CellState.ALIVE;
                }
                else{
                    return CellState.DEAD;
                }
            }
            return CellState.DEAD;
        }


    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}

