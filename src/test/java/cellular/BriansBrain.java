package cellular;
import java.util.Random;
import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {
	IGrid currentGeneration;

	@Override
	public CellState getCellState(int row, int column) {
		return null;
	}

	@Override
	public void initializeCells() {

	}

	@Override
	public void step() {

	}

	@Override
	public CellState getNextCell(int row, int col) {
		return null;
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns() ;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
